#!/bin/sh

do_clean() {
	rm -rf build
}

do_cmake() {
	mkdir -p build
	cd build
	cmake ..
	cd ..
}

do_build() {
	cd build
	make
}

case $1 in
	clean)
		do_clean
		;;

	cmake)
		do_cmake
		;;

	build)
		do_build
		;;

	all)
		do_clean
		do_cmake
		do_build
		;;

	*)
		echo "Usage: $0 { clean | cmake | build | all }"
		exit 1
		;;
esac
