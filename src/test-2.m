#import "gtk/objective-gtk.h"


@interface WindowHandler : NSObject <GTKWidgetDeleteEventHandler, GTKWidgetDestroyHandler>

@end

@implementation WindowHandler

- (void)dealloc
{
	NSLog(@"#%i%@ [WindowHandler dealloc]", [self retainCount], self);
	[super dealloc];
}

- (gboolean)onDeleteEvent:(GTKWidget*)widget :(GdkEvent*) event
{
	NSLog(@"#%i%@ [WindowHandler onDeleteEvent]", [self retainCount], self);
	return FALSE;
}

- (void)onDestroy:(GTKWidget*)widget
{
	NSLog(@"#%i%@ [WindowHandler onDestroy]", [self retainCount], self);
	gtk_main_quit();
}

@end


@interface ButtonHandler : NSObject <GTKButtonClickHandler>
{
	GTKWindow* window;
}

@property (nonatomic, retain) GTKWindow* window;

@end

@implementation ButtonHandler

@synthesize window;

- (void)dealloc
{
	NSLog(@"#%i%@ [ButtonHandler dealloc]", [self retainCount], self);
	[window release];
	[super dealloc];
}

- (void)onClick:(GTKButton*)button
{
	NSLog(@"#%i%@ [ButtonHandler onClick]", [self retainCount], self);
	[window destroy];
}

@end


int main(int argc, char* argv[])
{
	[GTK init: &argc : &argv];

	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

	WindowHandler* windowHandler = [[WindowHandler alloc] init]; // windowHandler:1

	GTKWindow* window = [[GTKWindow alloc] initWithType: GTK_WINDOW_TOPLEVEL]; // window:1
	[window onDeleteEvent: windowHandler]; // windowHandler+1
	[window onDestroy: windowHandler]; // windowHandler+1
	[window setTooltipText: "Good Bye World"];
	[window setBorderWidth: 10];
	[window show];

	[windowHandler release]; // windowHandler-1

	g_assert([window retainCount] == 1);
	g_assert([windowHandler retainCount] == 2);

	NSLog(@"-------------------------------------------------");
	NSLog(@"#%i%@", [window retainCount], window);
	NSLog(@"#%i%@", [windowHandler retainCount], windowHandler);
	NSLog(@"-------------------------------------------------");

	ButtonHandler* buttonHandler = [[ButtonHandler alloc] init]; // buttonHandler:1
	buttonHandler.window = window; // window+1

	GTKButton* button = [[GTKButton alloc] initWithLabel: "Hello World"]; // button:1
	[button onClicked: buttonHandler]; // buttonHandler+1
	[window add: button]; // button+1
	[button show];

	[button release]; // button-1
	[buttonHandler release]; // buttonHandler-1

	g_assert([window retainCount] == 2);
	g_assert([button retainCount] == 1);
	g_assert([buttonHandler retainCount] == 1);

	NSLog(@"-------------------------------------------------");
	NSLog(@"#%i%@", [window retainCount], window);
	NSLog(@"#%i%@", [button retainCount], button);
	NSLog(@"#%i%@", [buttonHandler retainCount], buttonHandler);
	NSLog(@"-------------------------------------------------");

	[pool drain];

	[GTK main];

	return 0;
}
