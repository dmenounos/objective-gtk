/*
 * Copyright (C) 2010 Dimitris Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "common.h"

typedef struct _OCClosure OCClosure;

/**
 * OCClosure stands for "Objective-C Closure".
 * OCClosure extends GCClosure, which stands for "Gnome C Closure",
 * providing support for Objective-C method callbacks.
 */
struct _OCClosure
{
	GCClosure gcclosure;
	id source;
	id target;
	SEL method;
};

/**
 * Creates a new closure which invokes the method of the target instance.
 * 
 * Additionaly, keeps both the source and the target alive for the lifetime
 * of the closure by initially retaining them and eventually releasing them
 * with a finalization notifier.
 * 
 * oc_closure_new is the equivalent of g_cclosure_new.
 */
GClosure* oc_closure_new (id source, id target, SEL method, gpointer data);
