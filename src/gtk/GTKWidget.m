/*
 * Copyright (C) 2010 Dimitris Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "GTKWidget.h"
#import "gmarshal.h"

@implementation GTKWidget

- (id) initWithWidget:(GtkWidget*)theWidget
{
	self = [super initWithPeer:(GObject*)theWidget];
	DEBUG(@"#%i%@ [GTKWidget initWithWidget]", [self retainCount], self);
	return self;
}

// override
- (void) dealloc
{
	DEBUG(@"#%i%@ [GTKWidget dealloc]", [self retainCount], self);
	[super dealloc];
}

// override
- (GtkWidget*) peer
{
	return (GtkWidget*)peer;
}

- (void) destroy
{
	gtk_widget_destroy([self peer]);
	[self release];
}

- (void) show
{
	gtk_widget_show([self peer]);
}

- (void) showAll
{
	gtk_widget_show_all([self peer]);
}

- (void) hide
{
	gtk_widget_hide([self peer]);
}

- (gboolean) activate
{
	return gtk_widget_activate([self peer]);
}

- (void) grabFocus
{
	gtk_widget_grab_focus([self peer]);
}

- (void) path:(guint*)pathLength :(gchar**)path :(gchar**)pathReversed
{
	gtk_widget_path([self peer], pathLength, path, pathReversed);
}

@end

@implementation GTKWidget (Properties)

- (gboolean) canFocus
{
	return gtk_widget_get_can_focus([self peer]);
}

- (void) setCanFocus:(gboolean)canFocus
{
	gtk_widget_set_can_focus([self peer], canFocus);
}

- (gboolean) hasFocus
{
	return gtk_widget_is_focus([self peer]);
}

- (const gchar*) name
{
	return gtk_widget_get_name([self peer]);
}

- (void) setName:(const gchar*)name
{
	gtk_widget_set_name([self peer], name);
}

- (gchar*) tooltipText
{
	return gtk_widget_get_tooltip_text([self peer]);
}

- (void) setTooltipText:(const gchar*)tooltipText
{
	gtk_widget_set_tooltip_text([self peer], tooltipText);
}

@end

@implementation GTKWidget (Signals)

- (void) onDeleteEvent:(id<GTKWidgetDeleteEventHandler>)handler
{
	[self onDeleteEvent:handler :@selector(onDeleteEvent::) :NULL];
}

- (void) onDeleteEvent:(id)handler :(SEL)selector :(gpointer)data
{
	GClosure* closure = [GTK connect:"delete-event" from:self to:selector of:handler with:data];
	g_closure_set_meta_marshal(closure, NULL, oc_closure_marshal_BOOLEAN__BOXED);
}

- (void) onDestroy:(id<GTKWidgetDestroyHandler>)handler
{
	[self onDestroy:handler :@selector(onDestroy:) :NULL];
}

- (void) onDestroy:(id)handler :(SEL)selector :(gpointer)data
{
	GClosure* closure = [GTK connect:"destroy" from:self to:selector of:handler with:data];
	g_closure_set_meta_marshal(closure, NULL, oc_closure_marshal_VOID__VOID);
}

@end
