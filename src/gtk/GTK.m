/*
 * Copyright (C)2010 Dimitris Menounos
 *
 * This program is free software:you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option)any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "GTK.h"
#import "GTKObject.h"
#import "gclosure.h"
#import "gmarshal.h"

@implementation GTK

+ (void)init:(int*)argc :(char***)argv
{
	gtk_init(argc, argv);
}

+ (void)main
{
	gtk_main();
}

+ (GClosure*) connect:(char*)signal
                 from:(GTKObject*)source
                   to:(GCallback)func
                 with:(gpointer)data
{
	DEBUG(@"\tCREATING CLOSURE FOR SIGNAL: %s", signal);
	GClosure* closure = g_cclosure_new(func, data, NULL);
	g_signal_connect_closure([source peer], signal, closure, TRUE);
	return closure;
}

+ (GClosure*) connect:(char*)signal
                 from:(GTKObject*)source
                   to:(SEL)method
                   of:(id)target
                 with:(gpointer)data
{
	DEBUG(@"\tCREATING CLOSURE FOR SIGNAL: %s", signal);
	GClosure* closure = oc_closure_new(source, target, method, data);
	g_closure_set_marshal(closure, oc_closure_marshal_VOID__VOID);
	g_signal_connect_closure([source peer], signal, closure, TRUE);
	return closure;
}

@end
