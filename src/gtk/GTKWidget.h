/*
 * Copyright (C) 2010 Dimitris Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "GTKObject.h"
#import "GTKWidget-handlers.h"

@interface GTKWidget : GTKObject

/**
 * Initializes with a native gtk widget to wrap.
 */
- (id) initWithWidget:(GtkWidget*)theWidget;

/**
 * Returns the wrapped gtk widget.
 */
- (GtkWidget*) peer;

- (void) destroy;

- (void) show;

- (void) showAll;

- (void) hide;

- (gboolean) activate;

- (void) grabFocus;

- (void) path:(guint*)pathLength :(gchar**)path :(gchar**)pathReversed;

@end

@interface GTKWidget (Properties)

- (gboolean) canFocus;

- (void) setCanFocus:(gboolean)canFocus;

- (gboolean) hasFocus;

- (const gchar*) name;

- (void) setName:(const gchar*)name;

- (gchar*) tooltipText;

- (void) setTooltipText:(const gchar*)tooltipText;

@end

@interface GTKWidget (Signals)

/**
 * Connects the "delete-event" signal to an event handler.
 */
- (void) onDeleteEvent:(id<GTKWidgetDeleteEventHandler>)handler;

/**
 * Connects the "delete-event" signal to a handler method with signature like:
 * (gboolean) handler:(GTKWidget*)source :(GdkEvent*)event :(gpointer)data;
 */
- (void) onDeleteEvent:(id)handler :(SEL)selector :(gpointer)data;

/**
 * Connects the "destroy" signal to an event handler.
 */
- (void) onDestroy:(id<GTKWidgetDestroyHandler>)handler;

/**
 * Connects the "destroy" signal to a handler method with signature like:
 * (void) handler:(GTKWidget*)source :(gpointer)data;
 */
- (void) onDestroy:(id)handler :(SEL)selector :(gpointer)data;

@end
