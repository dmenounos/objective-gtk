/*
 * Copyright (C) 2010 Dimitris Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "GTKWindow.h"

@implementation GTKWindow

- (id) initWithType:(GtkWindowType)type
{
	self = [self initWithContainer:(GtkContainer*)gtk_window_new(type)];
	DEBUG(@"#%i%@ [GTKWindow initWithType]", [self retainCount], self);
	return self;
}

// override
- (GtkWindow*) peer
{
	return (GtkWindow*) peer;
}

@end

@implementation GTKWindow (Properties)

- (const char*) title
{
	return gtk_window_get_title([self peer]);
}

- (void) setTitle:(const char*)title
{
	gtk_window_set_title([self peer], title);
}

- (gboolean) decorated
{
	return gtk_window_get_decorated([self peer]);
}

- (void) setDecorated:(gboolean)decorated
{
	gtk_window_set_decorated([self peer], decorated);
}

@end
