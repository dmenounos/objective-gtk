/*
 * Copyright (C) 2010 Dimitris Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "GTKButton.h"
#import "gmarshal.h"

@implementation GTKButton

- (id) init
{
	self = [self initWithWidget:gtk_button_new()];
	DEBUG(@"#%i%@ [GTKButton init]", [self retainCount], self);
	return self;
}

- (id) initWithLabel:(const char*)label
{
	self = [self initWithWidget:gtk_button_new_with_label(label)];
	DEBUG(@"#%i%@ [GTKButton initWithLabel]", [self retainCount], self);
	return self;
}

// override
- (GtkButton*) peer
{
	return (GtkButton*) peer;
}

@end

@implementation GTKButton (Properties)

- (const char*) label
{
	return gtk_button_get_label([self peer]);
}

- (void) setLabel:(const char*)label
{
	gtk_button_set_label([self peer], label);
}

@end

@implementation GTKButton (Signals)

- (void) onClicked:(id<GTKButtonClickHandler>)handler
{
	[self onClicked:handler :@selector(onClick:) :NULL];
}

- (void) onClicked:(id)handler :(SEL)selector :(gpointer)data
{
	GClosure* closure = [GTK connect:"clicked" from:self to:selector of:handler with:data];
	g_closure_set_meta_marshal(closure, NULL, oc_closure_marshal_VOID__VOID);
}

@end
