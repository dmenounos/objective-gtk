/*
 * Copyright (C) 2010 Dimitris Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "GTKContainer.h"
#import "gmarshal.h"

@implementation GTKContainer

- (id) initWithContainer:(GtkContainer*)theContainer
{
	if ((self = [super initWithWidget:(GtkWidget*)theContainer])) {
		children = [[NSMutableArray alloc] init];
	}

	DEBUG(@"#%i%@ [GTKContainer initWithContainer]", [self retainCount], self);
	return self;
}

// override
- (void) dealloc
{
	DEBUG(@"#%i%@ [GTKContainer dealloc]", [self retainCount], self);
	[children release];
	[super dealloc];
}

// override
- (GtkContainer*) peer
{
	return (GtkContainer*) peer;
}

- (void) add:(GTKWidget*)child
{
	gtk_container_add([self peer], [child peer]);
	[children addObject:child];
}

- (void) remove:(GTKWidget*)child
{
	gtk_container_remove([self peer], [child peer]);
	[children removeObject:child];
}

@end

@implementation GTKContainer (Properties)

- (guint) borderWidth
{
	return gtk_container_get_border_width([self peer]);
}

- (void) setBorderWidth:(guint)borderWidth
{
	gtk_container_set_border_width([self peer], borderWidth);
}

@end

@implementation GTKContainer (Signals)

- (void) onAdd:(id)object :(SEL)selector :(gpointer)data
{
	GClosure* closure = [GTK connect:"add" from:self to:selector of:object with:data];
	g_closure_set_meta_marshal(closure, NULL, oc_closure_marshal_VOID__OBJECT);
}

- (void) onRemove:(id)object :(SEL)selector :(gpointer)data
{
	GClosure* closure = [GTK connect:"remove" from:self to:selector of:object with:data];
	g_closure_set_meta_marshal(closure, NULL, oc_closure_marshal_VOID__OBJECT);
}

@end
