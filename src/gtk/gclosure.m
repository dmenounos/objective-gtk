/*
 * Copyright (C) 2010 Dimitris Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "gclosure.h"

static void oc_closure_destroy (gpointer data,  GClosure* gclosure)
{
	OCClosure* occlosure = (OCClosure*) gclosure;
	id target = occlosure->target;

	DEBUG(@"\toc_closure_destroy -- #%i%@", [target retainCount], target);

	[target release];
}

GClosure* oc_closure_new (id source, id target, SEL method, gpointer data)
{
	DEBUG(@"\toc_closure_new ++ #%i%@", [target retainCount], target);

	IMP callback = [target methodForSelector: method];
	GCallback callback_func = (GCallback) callback;

	g_return_val_if_fail(callback_func != NULL, NULL);

	GClosure* gclosure = g_closure_new_simple(sizeof(OCClosure), data);
	g_closure_add_finalize_notifier(gclosure, data, oc_closure_destroy);

	GCClosure* gcclosure = (GCClosure*) gclosure;
	gcclosure->callback = (gpointer) callback_func;

	OCClosure* occlosure = (OCClosure*) gclosure;
	occlosure->source = source;
	occlosure->target = target;
	occlosure->method = method;

	[target retain];

	return gclosure;
}
