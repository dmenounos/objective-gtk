/*
 * Copyright (C) 2010 Dimitris Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "GTKWidget.h"

@interface GTKContainer : GTKWidget
{
	NSMutableArray* children;
}

/**
 * Initialize with a native gtk container to wrap.
 */
- (id) initWithContainer:(GtkContainer*)theContainer;

/**
 * Returns the wrapped gtk container.
 */
- (GtkContainer*) peer;

- (void) add:(GTKWidget*)child;

- (void) remove:(GTKWidget*)child;

@end

@interface GTKContainer (Properties)

- (guint) borderWidth;

- (void) setBorderWidth:(guint)borderWidth;

@end

@interface GTKContainer (Signals)

/**
 * Connects the "add" signal to an object method with signature like:
 * (void) handler:(GTKContainer*)source :(GtkWidget*)widget :(gpointer)data;
 */
- (void) onAdd:(id)object :(SEL)selector :(gpointer)data;

/**
 * Connects the "remove" signal to an object method with signature like:
 * (void) handler:(GTKContainer*)source :(GtkWidget*)widget :(gpointer)data;
 */
- (void) onRemove:(id)object :(SEL)selector :(gpointer)data;

@end
