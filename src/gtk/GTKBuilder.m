/*
 * Copyright (C) 2010 Dimitris Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "GTKBuilder.h"

@implementation GTKBuilder

- initFromFile: (char *) filename {
	self = [super init];
	builder = gtk_builder_new();
	gtk_builder_add_from_file(builder, filename, NULL);
	return self;
}

/*
- (GtkObject *) getGtkObjectWithName: (char *) name {
	return GTK_OBJECT(gtk_builder_get_object(builder, name));
}
*/

- (void) dealloc {
	g_object_unref(G_OBJECT(builder));
	[super dealloc];
}

@end
