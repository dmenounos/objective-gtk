/*
 * Copyright (C) 2010 Dimitris Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "GTKObject.h"
#import "gmarshal.h"

@implementation GTKObject

- (id) initWithPeer:(GObject*)thePeer
{
	if ((self = [super init])) {
		g_object_ref_sink(G_OBJECT(thePeer));
		peer = thePeer;
	}

	DEBUG(@"#%i%@ [GTKObject initWithPeer] %s", [self retainCount], self, G_OBJECT_TYPE_NAME(thePeer));
	return self;
}

// override
- (void) dealloc
{
	DEBUG(@"#%i%@ [GTKObject dealloc]", [self retainCount], self);
	g_object_unref(G_OBJECT(peer));
	[super dealloc];
}

- (GObject*) peer
{
	return peer;
}

@end

@implementation GTKObject (Signals)

- (void) onNotify:(id)object :(SEL)selector :(gpointer)data
{
	GClosure* closure = [GTK connect:"notify" from:self to:selector of:object with:data];
	g_closure_set_meta_marshal(closure, NULL, oc_closure_marshal_VOID__PARAM);
}

@end
