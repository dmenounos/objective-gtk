@class GTKWidget;

@protocol GTKWidgetDeleteEventHandler

- (gboolean) onDeleteEvent:(GTKWidget*)widget :(GdkEvent*)event;

@end

@protocol GTKWidgetDestroyHandler

- (void) onDestroy:(GTKWidget*)widget;

@end
