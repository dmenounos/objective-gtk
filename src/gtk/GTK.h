/*
 * Copyright (C)2010 Dimitris Menounos
 *
 * This program is free software:you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option)any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "common.h"

@class GTKObject;

@interface GTK : NSObject

+ (void)init:(int*)argc :(char***)argv;

+ (void)main;

/**
 * Connects a signal to a function.
 *
 * @param signal the signal name
 * @param source the ObjC source
 * @param func the callback function
 */
+ (GClosure*) connect:(char*)signal
                 from:(GTKObject*)source
                   to:(GCallback)func
                 with:(gpointer)data;

/**
 * Connects a signal to an object method.
 *
 * @param signal the signal name
 * @param source the ObjC source
 * @param method the callback method
 * @param target the callback ObjC target
 */
+ (GClosure*) connect:(char*)signal
                 from:(GTKObject*)source
                   to:(SEL)method
                   of:(id)target
                 with:(gpointer)data;

@end
