/*
 * Copyright (C) 2010 Dimitris Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#import "GTK.h"

@interface GTKObject : NSObject
{
	GObject* peer;
}

/**
 * Initializes with a native gtk object to wrap.
 */
- (id) initWithPeer:(GObject*)thePeer;

/**
 * Returns the wrapped gtk object.
 */
- (GObject*) peer;

@end

@interface GTKObject (Signals)

/**
 * Connects the "notify" signal to an object method with signature like:
 * (void) handler:(GTKObject*)source :(GParamSpec*)paramSpec :(gpointer)data;
 */
- (void) onNotify:(id)object :(SEL)selector :(gpointer)data;

@end
