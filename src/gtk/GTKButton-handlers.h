@class GTKButton;

@protocol GTKButtonClickHandler

- (void) onClick:(GTKButton*)button;

@end
