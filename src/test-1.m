#import "gtk/objective-gtk.h"


@interface DummyHandler : NSObject

@end

@implementation DummyHandler

// override
- (void)dealloc
{
	NSLog(@"#%i%@ [DummyHandler dealloc]", [self retainCount], self);
	[super dealloc];
}

- (void)dummyHandler {}

@end


/**
 * Check plain widget reference count.
 */
static void testWidgetRetainCount()
{
	printf("\n");

	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

	GTKWindow* window = [[GTKWindow alloc] initWithType: GTK_WINDOW_TOPLEVEL];

	g_assert([window retainCount] == 1);

	[window destroy];

	[pool drain];
}


/**
 * Check widget with handler reference count.
 */
static void testWidgetWithHandlerRetainCount()
{
	printf("\n");

	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

	DummyHandler* handler = [[DummyHandler alloc] init];

	GTKWindow* window = [[GTKWindow alloc] initWithType: GTK_WINDOW_TOPLEVEL];
	[window onDestroy: handler : @selector(dummyHandler) : NULL];

	[handler release];

	g_assert([window retainCount] == 1);
	g_assert([handler retainCount] == 1);

	[window destroy];

	[pool drain];
}


/**
 * Check parent and child widget reference count.
 */
static void testParentChildWidgetRetainCount()
{
	printf("\n");

	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

	GTKButton* button = [[GTKButton alloc] initWithLabel: ""];

	GTKWindow* window = [[GTKWindow alloc] initWithType: GTK_WINDOW_TOPLEVEL];
	[window add: button];

	[button release];

	g_assert([window retainCount] == 1);
	g_assert([button retainCount] == 1);

	[window destroy];

	[pool drain];
}


int main(int argc, char* argv[])
{
	[GTK init: &argc : &argv];

	g_test_init(&argc, &argv, NULL);
	g_test_add_func("/testWidgetRetainCount", testWidgetRetainCount);
	g_test_add_func("/testWidgetWithHandlerRetainCount", testWidgetWithHandlerRetainCount);
	g_test_add_func("/testParentChildWidgetRetainCount", testParentChildWidgetRetainCount);

	return g_test_run();
}
