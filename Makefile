# gather source list (for link)
SOURCES = $(shell find src/gtk -name "*.m")

# create object and dependency lists (for link)
OBJECTS = $(patsubst src/%.m, obj/%.o, $(SOURCES))
DEPENDS = $(patsubst obj/%.o, obj/%.d, $(OBJECTS))

# GTK, GNUstep compile & link options
CFLAGS = `pkg-config --cflags gtk+-3.0` `gnustep-config --objc-flags` -std=c99
LFLAGS = `pkg-config --libs gtk+-3.0` `gnustep-config --base-libs`

CC = gcc

all : bin/test-1 bin/test-2

# link executable
bin/test-1 : obj/test-1.o $(OBJECTS)
	@test -d $(@D) || mkdir -p $(@D)
	$(CC) -o $@ $^ $(LFLAGS)

# link executable
bin/test-2 : obj/test-2.o $(OBJECTS)
	@test -d $(@D) || mkdir -p $(@D)
	$(CC) -o $@ $^ $(LFLAGS)

# compile sources
obj/%.o : src/%.m
	@test -d $(@D) || mkdir -p $(@D)
	$(CC) $(CFLAGS) -o $@ -c $<

-include $(DEPENDS)

clean :
	rm -rf bin obj

# $@    : The target of the current rule.
# $<    : The first dependency of the target.
# $^    : All the dependencies of the target.
# $(@D) : The directory part of the target, with the trailing slash removed.
